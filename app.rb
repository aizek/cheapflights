require 'sinatra/base'
require 'haml'
require 'pry'
require 'json'
require 'typhoeus'
require 'concurrent'

class App < Sinatra::Base
  set :public_folder, File.dirname(__FILE__) + '/static'
  set :cache, Concurrent::Hash.new()
  set :port, 3000
  CACHE_LIFETIME_SECONDS = 60

  API_BASE_URL = 'http://node.locomote.com/code-task'

  get '/' do
    haml :index
  end

  get '/api/airports' do
    content_type :json
    res = Typhoeus.get(API_BASE_URL + '/airports?q=' + params['q'], followlocation: true)
    if res.code == 200
      airports_list = JSON.parse(res.body)
      {
        airports: airports_list.map { |airport| { id: airport['airportCode'], name: "#{airport['airportName']}, #{airport['cityName']}, #{airport['countryName']}" } }
      }.to_json
    else
      status 500
    end
  end

  get '/api/search' do
    content_type :json
    response = Typhoeus.get(API_BASE_URL + '/airlines')
    unless response.code == 200
      status 500
      return
    end

    airline_search_urls = JSON.parse(response.body).map do |airline|
      API_BASE_URL + "/flight_search/#{airline['code']}?#{URI.encode_www_form(params)}"
    end
    responses = get_urls_multithread(airline_search_urls)

    flights = responses.inject([]) {|result, cur| result.concat(cur)}.sort_by{|flight| flight['price']}
    App.cache[:price_flight_cache] ||= {}
    set_uptodate_value_to(App.cache[:price_flight_cache], flights.first['price'], params['date'], params['from'], params['to'])
    result = ((Date.parse( params['date']) - 2)..(Date.parse( params['date']) + 2)).map do |day|
      day_info = {
        day: day.to_s,
        lowest: get_uptodate_value_from(App.cache[:price_flight_cache], day.to_s, params['from'], params['to'])
      }
      day_info[:flights] = flights if params['date'] == day.to_s
      day_info
    end
    {
      result: result
    }.to_json
  end



  def set_uptodate_value_to(object, value, *path)
    set_value_by_path(object, value, *(path + [:value]))
    set_value_by_path(object, Time.now, *(path + [:time]))
  end

  def set_value_by_path(destination, value, *path)
    path.each_with_index.inject(destination) do |object, (cur_path, ind)|
      if ind == path.length - 1
        object[cur_path] = value
      else
        object[cur_path] ||= {}
      end
    end
  end

  def get_uptodate_value_from(object, *path)
    cache_time = get_value_by_path(object, *(path + [:time]))
    if cache_time && (Time.now - cache_time) < CACHE_LIFETIME_SECONDS
      return get_value_by_path(object, *(path + [:value]))
    else
      return nil
    end
  end

  def get_value_by_path(object, *path)
    path.inject(object) { |object, cur_path| object && object[cur_path] }
  end

  def get_urls_multithread(urls)
    hydra = Typhoeus::Hydra.new
    requests = urls.map do |url|
      request = Typhoeus::Request.new(url, followlocation: true)
      hydra.queue(request)
      request
    end
    hydra.run
    requests.map { |request| JSON.parse(request.response.body) }
  end
end

App.run!