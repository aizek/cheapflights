$(function() {
  $(".datepicker-powered").datepicker({ dateFormat: "yy-mm-dd" });

  class DateTabs {
    constructor(api_payload) {
      this.date_tabs_template = Handlebars.compile($("#date-tabs-template").html());
      this.loading_template = Handlebars.compile($("#loading-template").html());
      this.error_template = Handlebars.compile($("#error-template").html());
      Handlebars.registerPartial('flightList', $("#flight-list-template").html());
    }

    renderTo(container_id, flights_info, todate) {
      flights_info.result = _.sortBy(flights_info.result, function(e){ return moment(e.day);})
      _.each(flights_info.result, function(e){ e.active = (e.day == todate) });
      $(container_id).html(this.date_tabs_template(flights_info));
    }

    renderLoadingTo(container_id) {
      $(container_id).html(this.loading_template());
    }

    renderErrorTo(container_id) {
      $(container_id).html(this.error_template());
    }
  };

  class DelayedApiSearch {
    constructor(forInput, apiUrl) {
      this.element = $(forInput);
      this.apiUrl = apiUrl;
      let search_object = this;

      this.element.keyup(function( event ) {
        if ($( this ).val().length < 4)
          return;
        if (search_object.timeoutID !== null || typeof(search_object.timeoutID) !== undefined)
          clearTimeout(search_object.timeoutID);
        search_object.timeoutID = setTimeout(search_object.perform_search.bind(search_object), DelayedApiSearch.DELAY_INTERVAL);
      });
    }

    perform_search() {
      this.timeoutID = null;
      $.ajax({
        url: DelayedApiSearch.API_BASE_URL + this.apiUrl + '?q=' + this.element.val()
      }).done(function(result) {
        var ul_drop = this.element.siblings('.dropdown-menu'), list;
        list = _.reduce(result.airports, function(result_html, element) {
          return result_html+'<li><a data-value=' + element.id + '>' + element.name + '</a></li>';
        }, "");
        ul_drop.html(list);
        ul_drop.find('a').click(function(event) {
          this.element.val(event.target.text);
          this.element.data('value', $(event.target).data('value'));
        }.bind(this));
        ul_drop.dropdown('toggle');
      }.bind(this));
    }
  };
  DelayedApiSearch.DELAY_INTERVAL = 500;
  DelayedApiSearch.API_BASE_URL = 'api/';

  $('#flight-search-form').submit(function( event ) {
    event.preventDefault();
    var todate = $('#date').val();
    search_query = '?from=' + $('#from').data('value') + '&to=' + $('#to').data('value') + '&date=' + todate;
    dateTabs.renderLoadingTo('#search-result-section');

    $.ajax({
      url: DelayedApiSearch.API_BASE_URL + 'search' + search_query
    })
    .done(function(result) {
      dateTabs.renderTo('#search-result-section', result, todate);
    }.bind(this))
    .fail(function() {
      dateTabs.renderErrorTo('#search-result-section');
    });
  });

  window.dateTabs = new DateTabs();
  window.FromSearcher = new DelayedApiSearch('#from', 'airports');
  window.ToSearcher = new DelayedApiSearch('#to', 'airports');
  Handlebars.registerHelper('extractAndFormatTime', function(context, block) {
    if (!window.moment) {
      return context;
    };
    var f = block.hash.format || "h:mm a";
    return moment(context).format(f);
  });
  Handlebars.registerHelper('extractAndFormatDate', function(context, block) {
    if (!window.moment) {
      return context;
    };
    var f = block.hash.format || "YYYY-MM-DD";
    return moment(context).format(f);
  });
  Handlebars.registerHelper('durationInHoursMins', function(context, block) {
    var result = (context % 60).toString() + ' minutes';
    if (context / 60 >= 1) {
      result = (Math.floor(context / 60).toString() + ' hours ').concat(result)
    }
    return result;
  });
  Handlebars.registerHelper('cutFractions', function(context, block) {
    return context.toFixed();
  });
});
